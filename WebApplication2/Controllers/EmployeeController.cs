﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using WebApplication2.Repository;

namespace WebApplication2.Controllers
{
    public class EmployeeController : Controller
    {
       // GET: Employee/GetAllEmpDetails    
        public ActionResult GetAllEmpDetails()
        {
            EmpRepository EmpRepo = new EmpRepository();
            ModelState.Clear();
            return View(EmpRepo.GetAllEmployees());
        }
        //GET:Employee/AddEmployee
        public ActionResult AddEmployee()
        {
            EmpRepository EmpRepo = new EmpRepository();
            ViewBag.State = EmpRepo.GetAllStates();
            return View();
        }

        //POST:Employee/AddEmployee
        [HttpPost]
        public ActionResult AddEmployee(EmpModel Emp)
        {
            EmpRepository EmpRepo = new EmpRepository();
            ViewBag.State = EmpRepo.GetAllStates();
            //ViewBag.District = EmpRepo.GetDistrictName();//edit
            if (EmpRepo.AddEmployee(Emp))
              {
               ViewBag.Message = "Employee details added sucessfully ";
               ModelState.Clear();
              }
            return View();
          
        }
        //GET: Employee/EditEmpDetails/id
        public ActionResult EditEmpDetails(int id)
        {
            EmpRepository EmpRepo = new EmpRepository();
            var empDetails = EmpRepo.GetAllEmployeesById(id);
            //GetDistrictName(id)
            var states = EmpRepo.GetAllStates();
            var statesItems = new List<SelectListItem>();
            foreach (var state in states) {
                statesItems.Add(new SelectListItem()
                {
                    Text = state.StateName,
                    Value = state.StateId.ToString(),
                    Selected = state.StateId.ToString() == empDetails.state ? true : false
                });
            }
            empDetails.StateList = statesItems;
            //empDetails //for districts 
            var district = EmpRepo.GetDistrictName(empDetails.state);
            var districtItems = new List<SelectListItem>();
            foreach (var dis in district)
            {
                districtItems.Add(new SelectListItem()
                {
                    Text = dis.DistrictName,
                    Value = dis.DistrictId.ToString(),
                    Selected = dis.DistrictId.ToString() == empDetails.District ? true : false
                });
            }
            empDetails.DistrictList = districtItems;
            //ViewBag.State =  EmpRepo.GetAllStates
            return View(empDetails);
        }
        // POST: Employee/EditEmpDetails/id  
        [HttpPost]
        public ActionResult EditEmpDetails(int id, EmpModel obj)
        {
            try
            {
                EmpRepository EmpRepo = new EmpRepository();
                EmpRepo.UpdateEmployee(obj);
                return RedirectToAction("GetAllEmpDetails");
            }
            catch
            {
                return RedirectToAction("GetAllEmpDetails");
            }
        }
        // GET: Employee/DeleteEmp/id   
        public ActionResult DeleteEmp(int id)
        {
            try
            {
                EmpRepository EmpRepo = new EmpRepository();
                if (EmpRepo.DeleteEmployee(id))
                {
                    ViewBag.AlertMsg = "Employee details deleted successfully";

                }
                return RedirectToAction("GetAllEmpDetails");
            }
            catch
            {
                return View();
            }
        }
        //[HttpPost]
        //public JsonResult GetAllStates()
        //{
        //    EmpRepository EmpRepo = new EmpRepository();
        //    var state = EmpRepo.GetAllStates();
        //    ModelState.Clear();
        //    return Json(state, JsonRequestBehavior.AllowGet);
        //}
        //[HttpPost]
        public JsonResult GetAllDistrictByState(String id)
        {
            EmpRepository EmpRepo = new EmpRepository();
            List<District> st = EmpRepo.GetDistrictName(id);
            return Json(st, JsonRequestBehavior.AllowGet);
        }
       
    }
 }