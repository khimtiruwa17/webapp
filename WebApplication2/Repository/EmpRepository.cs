﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using WebApplication2.Controllers;
using WebApplication2.Models;

namespace WebApplication2.Repository
{
    public class EmpRepository
    {
       private SqlConnection con;
       private void conncetion()
        {
            string constr = ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            con = new SqlConnection(constr);
        }
        public bool AddEmployee(EmpModel obj)
        {
            conncetion();
            SqlCommand com = new SqlCommand("addnewEmployee", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@name", obj.Name);
            //com.Parameters.AddWithValue("@city", obj.City);
            //com.Parameters.AddWithValue("@address", obj.Address);
            com.Parameters.AddWithValue("@state", obj.state); // state name instead of stateId
            com.Parameters.AddWithValue("@district",obj.District);
            com.Parameters.AddWithValue("@dob", obj.DOB);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if(i>=1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // view employess details
        public List<EmpModel> GetAllEmployees()
        {
            conncetion();
            List<EmpModel> EmpList = new List<EmpModel>();
            SqlCommand com = new SqlCommand("getEmployees", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
          //bind emp model generic list using datarow
            foreach(DataRow dr in dt.Rows)
            {
                EmpList.Add(
                    new EmpModel
                    {
                       Empid = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        //City = Convert.ToString(dr["City"]),
                        //Address = Convert.ToString(dr["Address"]),
                        state = Convert.ToString(dr["State"]),
                        District = Convert.ToString(dr["District"]),
                        DOB = Convert.ToString(dr["Dob"])
                    });
            }
           return EmpList;
        }

        public EmpModel GetAllEmployeesById(int id)
        {
            conncetion();
            List<EmpModel> EmpList = new List<EmpModel>();
            SqlCommand com = new SqlCommand("getEmployeesById", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmpId", id);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            //bind emp model generic list using datarow
            EmpModel empModel = new EmpModel();
            foreach (DataRow dr in dt.Rows)
            {
                empModel.Empid = Convert.ToInt32(dr["Id"]);
                empModel.Name = Convert.ToString(dr["Name"]);
                //empModel.City = Convert.ToString(dr["City"]);
                //empModel.Address = Convert.ToString(dr["Address"]);
                empModel.state = Convert.ToString(dr["State"]);
                empModel.District = Convert.ToString(dr["District"]);
                empModel.DOB = Convert.ToString(dr["Dob"]);
            }
            return empModel;
        }

        // updaate employee
        public bool UpdateEmployee(EmpModel obj)
        {
            conncetion();
            SqlCommand com = new SqlCommand("updateEmployee", con);

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmpId",obj.Empid);
            com.Parameters.AddWithValue("@name", obj.Name);
            //com.Parameters.AddWithValue("@city", obj.City);
            com.Parameters.AddWithValue("@state", obj.state); // stateId
            com.Parameters.AddWithValue("@district",obj.District);
            com.Parameters.AddWithValue("@dob", obj.DOB);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        // delete emp records
        public bool DeleteEmployee(int Id)
        {
            conncetion();
            SqlCommand com = new SqlCommand("DeleteEmpByID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmpId", Id);

            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<State> GetAllStates()
        {
            conncetion();
            List<State> states = new List<State>();
            SqlCommand com = new SqlCommand("getState", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow drow in dt.Rows)
            {
                states.Add(new State
                {
                    StateId = Convert.ToInt32(drow["StateId"]),
                    StateName = Convert.ToString(drow["StateName"])
                });
            }
            con.Close();
            return states;
        }

        public List<District> GetDistrictName(string id)
        {
            conncetion();
            List<District> districts = new List<District>();
            SqlCommand com = new SqlCommand("getDistrict", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@StateId", id);
            SqlDataAdapter da = new SqlDataAdapter(com);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow drow in dt.Rows)
            {
                districts.Add(new District
                {
                   DistrictId = Convert.ToInt32(drow["DistrictId"]),
                   DistrictName = Convert.ToString(drow["DistrictName"])
                });
            }
            con.Close();
            return districts;
        }
       
    }
}