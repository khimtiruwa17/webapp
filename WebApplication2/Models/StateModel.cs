﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Controllers
{
    public class State
    {
        public int StateId { get; set; }
        public string StateName { get; set;}
        
    }
    public class District
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        //public int StateId { get; set; }
    }
}