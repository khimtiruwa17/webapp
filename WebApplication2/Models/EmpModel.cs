﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Models
{
    public class EmpModel
    {
        [Display(Name = "Id")]
        public int Empid { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        //[FullName]
        //[Display(Name = "Name")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "City is required.")]
        //public string City { get; set; }

        //[Required(ErrorMessage = "Address is required.")]
        //public string Address { get; set; }
        public string state { get; set; }
        public string District { get; set; }

        [Required]
        [Display(Name = "Date Of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public string DOB { get; set; }
        public List<SelectListItem> StateList {get; set;}

        public List<SelectListItem> DistrictList {get; set;}
    }
}