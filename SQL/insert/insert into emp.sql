USE [crud]
GO
/****** Object:  StoredProcedure [dbo].[addnewEmployee]    Script Date: 8/19/2021 11:51:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[addnewEmployee](
@name varchar(50),
--@city varchar(50),
--@address varchar(50),
@state varchar(50),
@district varchar(50),
@dob date
) 
as 
begin 
	--insert into Employee(Name,City,Address,state,district,dob) 
	--VALUES (@name,@city,@address,@state,@district,@dob)
		insert into Employee(Name,state,District,dob) 
	VALUES (@name,@state,@district,@dob)
end