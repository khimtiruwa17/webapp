create procedure getDistrict(
@stateId int
)
as 
begin
select DistrictName, DistrictId
from Settings.Districts 
where StateId =@stateId
end