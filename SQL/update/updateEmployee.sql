create procedure updateEmployee(
@EmpId int,
@name varchar(50),
@city varchar(50),
@state varchar(50),
@district varchar(50)
)
as 
begin 
update Employee
set Name = @name,
City = @city,
State =@state,
District=@district

where Id = @EmpId
End