USE [crud]
GO
/****** Object:  StoredProcedure [dbo].[updateEmployee]    Script Date: 8/19/2021 11:56:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[updateEmployee](
@EmpId int,
@name varchar(50),
--@city varchar(50),
@state varchar(50),
@district varchar(50),
@dob date
)
as 
begin 
update Employee
set Name = @name,
--City = @city,
State =@state,
District=@district,
Dob=@dob


where Id = @EmpId
End