USE [crud]
GO
/****** Object:  StoredProcedure [dbo].[getEmployeesById]    Script Date: 19/08/2021 1:40:26 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[getEmployeesById](
@EmpId int
) 
as 
begin 
select Id,Name,State,District,CONVERT(VARCHAR,dbo.Employee.Dob,23)AS Dob from Employee where Id=@EmpId
end