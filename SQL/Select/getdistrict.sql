USE [crud]
GO
/****** Object:  StoredProcedure [dbo].[getDistrict]    Script Date: 17/08/2021 12:01:33 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[getDistrict]
AS
Begin
DECLARE @StateId int
SELECT  d.DistrictName, d.DistrictId
FROM Settings.Districts d
WHERE  d.StateId =@StateId;
End